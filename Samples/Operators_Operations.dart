//operators
void main() {
  int a = 3;
  int b = 2;
  var h = a ~/ b; // ~ return in intiger
  print("the quotient of $a and $b = $h");

  var i = (a + b) - (a * b);
  print("the aritmatic of ($a+$b) and ($a*$b) = $i");

  var k = a % b;
  print("The remainder of a and b is $k");

  var e = -b;
  print("The negation of difference between a and b is $e");

  if (e is int) {
    print("$e is int");
    if (e is! String) print("not string");
  }
  if (e is! String && k is int) {
    var e1 = a >> b; // bitwise shift // rarely used..
    var e2 = a & b;
    var e3 = b & a;
    print("$e1 $e2 $e3");
    print("$e $k $e1");
  }
  assignment_operator(4, 5);
  conditional_operators();
  CasCadeNotationOperator ops1 = new CasCadeNotationOperator(); //creating object of class
  CasCadeNotationOperator ops2 = new CasCadeNotationOperator();

  //Without using Cascade Notation
  ops1.set(5, 5);
  ops1.add();

  //Using cascade notation
  ops2..set(4, 7)..add();

  /*
  * Cascade Notation Operators:
    This class of operators allows you to perform a sequence of operation on the same element.
  *  It allows you to perform multiple methods on the same object.*/
}

void assignment_operator(int a1, int b2) {
  int a = a1;
  int b = b2;
  var c = a * b;
  var d; // null value
  d ??= a + b; // Value is assign as its null
  print("$d");
  print("in assignment_operator $c");
}

void conditional_operators() {
  print("in conditional_operators");
  int a = 5;
  int b = 8;
  var r;
  var c = (a >= 5)
      ? "this is correct 1" // do when condition is statisfied i.e if true and break
      : (b > 10) // else  c = (a < 5) CONDITION failed check this , i.e if perious condition is false
      ? "this right" // do when condition is statisfied i.e if (b > 10) true
      : r = (a + b); // do when condition failed...
  r = (b > 5) ? "this is correct" : "this is wrong";

  print("$c");
  print("$r");

  var nullchk;
  var data = nullchk ?? "nullchk is $nullchk";
  print(data);

  nullchk = 6;
  var data2 = nullchk ?? "nullchk is $nullchk";
  print(data2);
}

class CasCadeNotationOperator {
  var a;
  var b;

  void set(x, y) {
    this.a = x;
    this.b = y;
  }

  void add() {
    var z = this.a + this.b;
    print("addition is $z");
  }
}
