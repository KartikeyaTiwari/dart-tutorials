//simple flows
import 'dart:io';

void main() {
  if_else_condition();
  switch_condition();
  loopingloop();
}

void loopingloop() {
  for (int i = 0; i < 5; i++) {
    print("hi $i");
  }

  var tapatell = [1, "two", 3.45555];
  for (Object i in tapatell) {
    print("tapatell at $i");
  }
  tapatell.forEach((var num) => print(num));

  var data = 5;
  int i = 1;
  while (i < data) {
    print("its less");
    i++;
    if (i == 3) {
      print("i am broke");
      break;
    }
  }

  var data2 = 5;
  int j = 1;
  do {
    print("hi there");
    j++;
    if (j == 4) {
      print("hahaha continue");
      continue;
    }
  } while (j <= data2);

  mylablel:
  for (int i = 0; i < 10; i++) {
    if (i < 7) {
      print("$i is small");
    }
    if (i == 7) {
      print("we got 7");
      break mylablel;
    }
    print("inside loop");
  }
}

void switch_condition() {
  int num;
  print("enter num");
  num = int.parse(stdin.readLineSync()!);

  switch (num) {
    case 1:
      print("in 1st switch");
      break;
    case 2:
      print("in 2nd switch");
      break;
    default:
      {
        print("not an option");
      }
  }
}

void if_else_condition() {
  int num = 15;
  if (num > 10) {
    print("$num is greater than 10");
  } else {
    print("$num is smaller than 10");
  }

  //if elseif -else
  int num2 = 10;
  if (num2 < 9) {
    print("condition 1 true");
    num2++;
  } else if (num2 >= 10) {
    print("condition 2 true");
  } else {
    print("all false");
  }

  //single if condition
  if (num > num2) {
    print("$num is greater than $num2");
  }
  if (num < num2) {
    print("$num is smaller than $num2");
  }
}
