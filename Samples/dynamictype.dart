main(){
  dynamic a =5;
  print(a); //int
  a= 7.0;
  print(a.runtimeType); //double
  a='test';
  print(a.runtimeType);//string

  /*
  * static analysier is deactivated here since we are using dynamic
  * dynamic basically change as per the assignment
  * runtime check
  * */
}