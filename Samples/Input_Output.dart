//standard input - output
import 'dart:io'; //library for I/O operations

void main() {
  int? age;
  print("What is your name ?");
  String? name = stdin.readLineSync();
  print("Hello , $name \n welcome to dart...");

  // try catch block -> if enter string instead of int then try catch will handle the exception
  try {
    print("enter age?");
    age = int.parse(stdin.readLineSync()!); //? and ! for null safety
    print("your age is $age");
  } on Exception catch (e) {
    print("exception occured $e");
  }

  print("from print method writing $name ");
  stdout.write("From stdout.write writing $age");

  stdout.write("Enter 2 number ?");
  int? val = int.parse(stdin.readLineSync()!);
  int? val2 = int.parse(stdin.readLineSync()!);
  var sum = val + val2;
  print("addition is $sum");

}
