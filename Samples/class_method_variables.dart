class planet {
  var name; // var : type inferance
  var size = [11, 22, 33]; // var defined and hece cant be chage
  var gravity;
  var r3; // var not defined or assigned any values hence can be changed
  String a = "1";
  int b = 1;
  dynamic f;
  dynamic f1 = 5;
  bool vector = true;

  /*
  * dynamic: can change TYPE of the variable, & can change VALUE of the variable later in code.
  *  var: can't change TYPE of the variable,
  * but can change the VALUE of the variable later in code.*/

  callSize() {
    print(size);
  }

  bool checkeven(gravity) {
    f1 = "tt";
    if (gravity % 2 == 0) {
      print("true");
      print(r3); // from null
      if (r3 == null) {
        r3 = 'sttt'; // then string
        print(r3);
        r3 = 1; //then intiger
        r3++;
        print(r3);
        r3 = "grrk of geeks";
        print(r3);
        b++;
        print(b);
        if (b < 5) {
          vector = false;
          print(vector);
        }
      }
      return true;
    } else
      print("false");
    return false;
  }

  void checkgeek() {
    dynamic geek = "Geeks For Geeks";

    // Printing variable geek
    print(geek);

    // Reassigning the data to variable and printing it
    geek = 3.14157;
    print(geek);
    bool test;
    test = 12 > 5;
    print(test);
  }
}

void main() {
  planet p = new planet();
  p.callSize();
  p.checkeven(22);
  p.checkgeek();
  datatype_num_str();
  datatype_list();
  datatype_maps();
  /*
  * dynamic: can change TYPE of the variable, & can change VALUE of the variable later in code.
  *  var: can't change TYPE of the variable,
  * but can change the VALUE of the variable later in code.*/
}

void datatype_maps() {
  Map maps = new Map();
  maps['1']='my';
  maps[2]='plannet';
  maps['3']=9;
  print(maps);
}

void datatype_list() {
  List data = new List.filled(4, null, growable: false);
  data[0] = "my";
  data[1] = "ok";
  data[2] = "list";
  data[3] = 5;
  print("$data");
  print(data[0]);
  print(data[3]);
}

void datatype_num_str() {
  print("datatype : num , which inherits int and double only :)");
  //num type only applicable to int and double since it inherits from int and double type
  // NO string allowed in num type
  var a1 = num.parse("4");
  var a2 = num.parse("3.14");
  var a3 = a1 + a2;
  print("num type addition $a3");

  //str
  var gfg = r'This is a raw string';
  print(gfg);
  String s = "my";
  String s1 = "plannet";
  String s2 = s + s1;
  print("$s2");
}
