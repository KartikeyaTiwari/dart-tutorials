//checkout set list quees rewise data type :)
// data types
import 'dart:collection'; // for queue

void main() {
  type_list();
  type_set();
  type_map();
  type_queue();

  //type enum
  for (type_enum mytype in type_enum.values) {
    print(mytype);
  }
  var takeenum = type_enum.side_lower;
  switch (takeenum) {
    case type_enum.low:
      print("low");
      break;
    case type_enum.side_lower:
      print("side lower");
      break;
    case type_enum.medium:
    // TODO: Handle this case.
      break;
    case type_enum.high:
    // TODO: Handle this case.
      break;
    case type_enum.upper:
    // TODO: Handle this case.
      break;
    case type_enum.side_upper:
    // TODO: Handle this case.
      break;
  }
}

void type_list() {
  print(
      "______________________-type : LIST :same as array__________________________________-");
  List? mylist = List.filled(5, null, growable: true);
  //Unsupported operation: Cannot add to a fixed-length list if growable: false
  // and u try to add
  mylist[0] = 1;
  mylist[1] = 2;
  mylist[2] = "kartik";
  print(mylist); //print the list
  print(mylist[2]); //print the data at 2nd index , i.e : 0,1,2

  mylist.add("lol"); //adding data to list
  mylist.add(76);
  mylist[4] = "r";
  print(mylist);

  mylist.addAll([1, "composite", "jl50"]);
  print(mylist);

  //insertion at index
  mylist.insert(3, "foo bar");
  mylist.insert(4, "foo bar");
  print(mylist);
  print(mylist[5]);

  var q = mylist[6];
  print("$q at 6th");

  if (mylist[5] == null) {
    mylist.insert(5, 5);
    print(mylist);
  }
  print("$q at 6th");
  mylist.insertAll(7, [1, "composite", "jl50"]);
  print("$q at 6th");

  int a = 3;
  var newlist = List.generate(
      a, (int index) => List.generate(3, (hj) => a + hj),
      growable: false);
  var k = newlist;
  print("this is the new list $k");
}

void type_set() {
  print(
      "_____________________type : SET : for unique data________________________________________");
  /*
  * var variable_name = <variable_type>{};
 or,
 Set <variable_type> variable_name = {};*/
  var myset = <String>{
    "1",
    "2"
  }; //contain unique if u put 1,1 (same)then it will print only 1 and not 1,1
  print(myset);
  var data1 = ["kartik", 1, "calling", "kartik"]; // [] for list
  print("output of list = $data1");
  var data2 = {"kartik", 1, "calling", "kartik"}; // {} for set
  print("output of set = $data2");

  data2.add("value");
  print(data2);
  var namelist = {
    1,
    2,
    3,
    1
  }; //1 will not add since its aleready added (check for common data and its data type)
  data2.addAll(namelist);
  print("after add all $data2");

  int length = data2.length;
  print("set lenth is $length");
  bool check = data2.contains("value");
  print("The value of check is: $check");
  data2.remove(1);
  print(data2);

  // data2.forEach((element) {
  //     if(element == 2){
  //       print("found");
  //     }
  //     else{
  //       int index;
  //       //var a = data2.elementAt();
  //       print("object not found ");
  //     }
  // });

  for (int i = 0; i < data2.length; i++) {
    if (data2.elementAt(i) == 2) {
      print("found at $i");
    } else {
      print("object not found");
    }
  }
  print(data2);

  List<Object> list =
  data2.toList(); //type object since it contain both string and intiger
  print(list);

  // [] list ,{} set ,() maps
  var newtype = <String>{"a", "b", "c"};
  var maptype = newtype.map((e) => "$e");
  print("set to map : $maptype");
}

void type_map() {
  print(
      "____________________type : MAP_____key:value pair_______________________________________");
  // Creating the Map using Map Literals
  var mymap = {1: "22", "2": 245, 3: "3.555", 4.4: "fourtheen"};
  print(mymap);
  print(mymap["2"]);
  print(mymap[1]);
  print(mymap[0]);
  //Creating Map using Map Constructors
  var my2map = new Map();
  my2map[1] = "10";
  my2map[2] = "20";
  my2map[3] = "g";
  my2map[3] = "gf"; //this will replace the previous value
  print(my2map);
  print(my2map[1] + my2map[2]);
}

void type_queue() {
  print(
      "_________________________________type : QUEUE_____________________________________");
  //Using Constructor:
  Queue<String> myqueue = new Queue<String>();
  print(myqueue);
  myqueue.add("string type only");
  myqueue.add("value");
  myqueue.add("value1");
  print(myqueue);
  //creating queue through list
  List<Object> mylist = [1, "new data 1", 3.555555];
  Queue<Object> myq = new Queue<Object>.from(mylist);
  print(myq);
  //operations onqueue
  myq.add("value");
  print(myq);
  myq.clear();
  print(myq);
  List<int> mylistt = [1, 2, 3];
  myq.addAll(mylistt);
  print(myq);
  print(myq.isEmpty);
  myq.addFirst("new value");
  print(myq);
  myq.addLast(4);
  print(myq);
  print(myq.length);
  myq.remove(2);
  print(myq);
  myq.removeFirst();
  print(myq);
  myq.removeLast();
  print(myq);
  myq.forEach(print);
}

enum type_enum {
  /*"________________________________________type : ENUM____________________________________________________"
  * Enumerated types (also known as enumerations or enums) are primarily used to define named constant values.
  * The enum keyword is used to define an enumeration type in Dart.
  * The use case of enumeration is to store finite data members under the same type definition.
Syntax:
enum variable_name{
  // Insert the data members as shown
  member1, member2, member3, ...., memberN
}*/
  low,
  medium,
  high,
  upper,
  side_upper,
  side_lower
}
