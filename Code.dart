//functions
void main() {
  var output = add(10, 20);
  print(output);

  opp1(10, 4, 3, 2);
  onp2(20, j: 3, k: 5);
}

int add(int i, int j) {
  int result = i + j;
  return result;
}

//optional positional parameters  opp1(10,4,3,2);
void opp1(int i, [int? j, int? k, int? r]) {
  // making j nullable in case if its not passed
  print("i= $i and j= $j and k= $k and r= $r");
}

//optional named parameter
void onp2(int i, {int? j, int? k}) {
  //parameter must passed using name ex:  onp2(20,j: 3,k: 5);
  print("i= $i and j= $j and k= $k");
}
